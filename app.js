// "use strict";

let express = require('express')
let request = require('request')
let mung = require('express-mung')
let bodyParser = require('body-parser')
let _ = require('underscore')

let app = express()
app.use( bodyParser.json() );       // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
  extended: true
})); 
let router = express.Router();

let kvs = require('./kvs')

// middleware to add in ownder to requests
app.use(mung.json((body, req, res)=>{
	// TODO only insert owner if key get
	let key = req.params.key
	if(key != 'view_update') {
		body.owner = kvs.belongsTo(key)
	}
	return body
}))

router.use('/kvs/:key', (req, res, next)=>{
	res.type('json')
	let key = req.params.key
	if(key == 'view_update') next()
	if(req.method == 'PUT' && !req.body.val && key != 'view_update' && key != 'update') {
		res.status(404).json({
			msg: 'error',
			error: 'yarr be needin a key matey'
		})
	} else {
		next()
	}
})

router.param('key', (req, res, next, name)=>{
	let key = req.params.key
	if(key.length > 250) {
		res.status(400).json({
			msg: 'error',
			error: 'key too long'
		})
	} 
	// accept=1 forces node to accept regardless of hash
	// does not get forwarded to correct node
	if(req.query.accept) return next()
	if(key == 'view_update') return next()
	let belongsTo = kvs.belongsTo(key)
	if(belongsTo != kvs.nodeIpPort && req.query.accept != 1) {
		let addr = kvs.belongsTo(key)
		console.log('going to: ' + addr)
		// Forward the request
		let url = 'http://' + belongsTo + req.url
		console.log(url)
		request({
			uri: url,
			method: req.method,
			json: req.body 
		}).pipe(res)
	}	
	else {
		next()
	}

})


router.put('/kvs/view_update', (req, res)=>{
	let type = req.query.type
	let ipPort = req.body.ip_port
	let prepQueue = []
	let view = kvs.localNodeChange(type, ipPort)

	_.each(kvs.view, (node)=>{
		prepQueue.push(kvs.messageRemote(type, node, view, 'prepare'))
	})

	let runTasks = new Promise((resolve, reject) => {
		prepQueue.forEach((prep, i, arr)=>{
			prep.then(()=>{
				console.log('prepped ' + arr[i])

			})
			.catch(()=>{console.error('error prepping ' + arr[i])})

			if( i === arr.length -1 ) { resolve() }
		})
	})

	runTasks.then(()=>{
		// tell everyone else to hash
		kvs.view.forEach((node, i, arr)=>{
			request.get(`http://${node}/rehash`)
			.on('response', (resp)=>{
				console.log('rehashed returned ' + resp.statusCode)
			})
			if( i === arr.length -1 ) { res.send({
				msg: 'success'
			}) }
		}).catch((err)=>{
			console.error('view_update::error' + err)
		})
	})
	
})

router.put('/kvs/:key', (req, res)=>{
	let key = req.params.key
	if(key != 'view_update') {
		if(kvs.get(key)) {
			kvs.insert(key, req.body.val)
			res.status(201).json({
				replaced:1,
				msg: 'success'
			})
		} else {
			kvs.insert(key, req.body.val)
			res.status(201).json({
				replaced:0,
				msg: 'success'
			})
		}
	}
})

router.get('/kvs/:key', (req,res)=>{
	let key = req.params.key
	if(kvs.get(key)) {
		res.status(201).send({
			msg: 'success',
			value: kvs.get(key)
		})
	} else {
		res.status(404).send({
			msg: 'error',
			error: 'key does not exist'
		})
	}
})

router.delete('/kvs/:key', (req,res)=>{
	let key = req.params.key
	if(kvs.get(key)) {
		kvs.del(key)
		res.status(201).send({
			msg: 'success',
		})
	} else {
		res.status(404).send({
			msg: 'error',
			error: 'key does not exist'
		})
	}
})

router.get('/rehash', (req, res)=>{
	console.log('rehashed called')
	console.log(kvs.view)
	kvs.remoteNodeChange()
})

router.post('/update/:type', (req, res)=>{
	let phase = req.query.phase // either prepare or ready
	let type = req.params.type

	if(phase == 'prepare') {
		// add or remove node from local view
		kvs.setView(req.body.view)
		res.send()
	} else if(phase == 'ready') {
		// rehash/broadcast to remote nodes
		kvs.remoteNodeChange()
		res.send()
	}
})

app.use('/', router)

app.listen(8080, ()=>{
    console.log('Yay im listening on ' + 8080)
})
