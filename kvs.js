const crc32 = require('crc').crc32
let view = [];
if(process.env.VIEW){
	view = process.env.VIEW.split(',') // "10.0.0.20:8080,10.0.0.21:8080" -> ['10.0.0.20:8080','10.0.0.21:8080']
} else {
	view = view.push(process.env.IPPORT)
}
let nodeIpPort = process.env.IPPORT
let nodeCount = view.length
let kvs = {};
let exec = require('child_process').exec
let _ = require('underscore')
let reqp = require('request-promise')

// kvs node operations

function removeNode(node) {
	let index = view.indexOf(node)	
	view.splice(index, 1)
	nodeCount = view.length
}

function addNode(node) {
	if(view.indexOf(node) < 0) {
		view.push(node)
		nodeCount = view.length
	}
}

function localNodeChange(type, ipPort) {
	if(type == 'remove') {
		removeNode(ipPort)
	} else if(type == 'add') {
		addNode(ipPort)
	}
	return view
}

function setView(newView) {
	view = newView
}

// rehashes and redistributes the keys to proper nodes
function remoteNodeChange() {
	console.log('remoteNodeChange')
	console.log(kvs)
	// _.each(kvs, (key, value)=>{
	for(var key in kvs) {
		let to = belongsTo(key)
		if(to != nodeIpPort) {
			let url = `http://${to}/kvs/${key}`
			reqp({
				uri: url,
				method: 'PUT',
				json: {val: kvs[key]}
			}).then((res)=>{
				console.log("rehash: " + res)
				console.log(res)
				del(key)
			}).catch((err)=>{
				console.error('remoteNodeChange::error' + err)
			})
		}
	}
}

function messageRemote(type, node, view, phase) {
	console.log('messageRemote view:')
	console.log(view)
	console.log(`http://${node}/update/${type}?phase=${phase}`)
	return reqp({
		uri: `http://${node}/update/${type}?phase=${phase}`,
		method: 'POST',
		json: {view: view}
	})
}

function belongsTo(key) {
	return view[hash(key)]
}

function hash(key) {
	nodeCount = view.length
	console.log("hash::hash " + key)
	console.log('hash::nodeCount ' + nodeCount)

	let index = crc32(key) % nodeCount
	return index
}

// actual kvs operations

function insert(key, value) {
	kvs[key] = value
}

function get(key) {
	return kvs[key]
}

function del(key) {
	delete kvs[key]
}

function broadcastChange(nodeAddr, type, cb) {
	let everyoneExceptMe = view.slice(0)
	everyoneExceptMe.splice(everyoneExceptMe.indexOf(nodeIpPort), 1)
	console.log('everyoneExceptMe ' + everyoneExceptMe)
	_.each(everyoneExceptMe, (addr)=>{
		reqp({
			uri: `http://${addr}/update/${type}/${nodeAddr}`,
			method: 'GET'
		}).then((res)=>{
			console.log("broadcastChange: " + res)
			cb()
		}).catch((err)=>{
			// console.error('Error:' + err)
			console.error('Error:')
		})
	})
}

module.exports.view = view
module.exports.nodeIpPort = nodeIpPort
module.exports.nodeCount = nodeCount
module.exports.insert = insert
module.exports.get = get
module.exports.delete = del
module.exports.belongsTo = belongsTo
module.exports.addNode = addNode
module.exports.removeNode = removeNode
module.exports.broadcastChange = broadcastChange
module.exports.localNodeChange = localNodeChange
module.exports.remoteNodeChange = remoteNodeChange
module.exports.messageRemote = messageRemote
module.exports.setView = setView
