#!/bin/bash
docker run -d -p 8083:8080 --net=mynet --ip=10.0.0.3 -e VIEW="10.0.0.3:8080,10.0.0.4:8080" -e IPPORT="10.0.0.3:8080" hw3 &
docker run -d -p 8084:8080 --net=mynet --ip=10.0.0.4 -e VIEW="10.0.0.3:8080,10.0.0.4:8080" -e IPPORT="10.0.0.4:8080" hw3 &
docker run -d -p 8085:8080 --net=mynet --ip=10.0.0.5 -e IPPORT="10.0.0.5:8080" hw3
