FROM node:7.4.0
MAINTAINER "aeroman@ucsc.edu"

RUN mkdir -p /usr/app 
WORKDIR /usr/app
COPY . /usr/app
RUN npm install
EXPOSE 8080
ENTRYPOINT ["node", "app.js"]
